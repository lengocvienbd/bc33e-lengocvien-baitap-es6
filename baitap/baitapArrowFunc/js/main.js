const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermil",
  "lion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
var contentHTML = "";
colorList.forEach((item) => {
  let content = `<button class="color-button ${item}" ></button>`;
  contentHTML += content;
});
document.getElementById("colorContainer").innerHTM = contentHTML;
let buttonList = document.querySelectorAll(".color-button");
document.querySelector(".pallet").classList.add("active");
document.getElementById("house").classList.add("pallet");
buttonList.forEach((button, indexButton) => {
  button.onclick = function () {
    document.querySelector(".color-button.active").classList.remove("active");
    this.classList.add("active");
    colorList.forEach((item, indexCoLor) => {
      if (indexButton == indexCoLor) {
        document.getElementById("house").classList.add(item);
      } else {
        document.getElementById("house").classList.remove(item);
      }
    });
  };
});
